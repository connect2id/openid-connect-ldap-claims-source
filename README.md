# Connect2id Server LDAP Claims Source

Copyright (c) Connect2id Ltd., 2013 - 2022

LDAP connector for sourcing OpenID Connect claims for a subject (end-user).

* Implements the `com.nimbusds.openid.connect.provider.spi.claims.ClaimsSource`
  SPI from the Connect2id server SDK.

* Supports retrieval of arbitrary LDAP attributes and mapping them to OpenID
  Connect claims.

* Supports multiple scripts and languages via language tags.


Example configuration properties:

```
op.ldapClaimsSource.enable = true

op.ldapClaimsSource.attributeMap = /WEB-INF/ldapClaimsMap.json

op.ldapClaimsSource.server.url = ldap://localhost:1389 ldap://remotehost:1389
op.ldapClaimsSource.server.selectionAlgorithm = FAILOVER
op.ldapClaimsSource.server.connectTimeout = 250
op.ldapClaimsSource.server.security = STARTTLS
op.ldapClaimsSource.server.trustSelfSignedCerts = true
op.ldapClaimsSource.server.connectionPoolSize = 10
op.ldapClaimsSource.server.connectionPoolMaxWaitTime = 250
op.ldapClaimsSource.server.connectionMaxAge = 0

op.ldapClaimsSource.directory.user.dn = cn=Directory Manager
op.ldapClaimsSource.directory.user.password = secret

op.ldapClaimsSource.directory.baseDN = ou=people,dc=wonderland,dc=net
op.ldapClaimsSource.directory.scope = ONE
op.ldapClaimsSource.directory.filter = (uid=%u)

op.ldapClaimsSource.customTrustStore.enable = false
op.ldapClaimsSource.customTrustStore.file =
op.ldapClaimsSource.customTrustStore.password =
op.ldapClaimsSource.customTrustStore.type =

op.ldapClaimsSource.customKeyStore.enable = false
op.ldapClaimsSource.customKeyStore.file =
op.ldapClaimsSource.customKeyStore.password =
op.ldapClaimsSource.customKeyStore.type =
```

Example JSON object specifying the mapping of LDAP attributes to OpenID Connect
claims:

```
{
  "sub"                    : { "ldapAttr" : "uid" },
  "name"                   : { "ldapAttr" : "cn", "langTag" : true },
  "given_name"             : { "ldapAttr" : "givenName", "langTag" : true },
  "family_name"            : { "ldapAttr" : "sn", "langTag" : true },
  "nickname"               : { "ldapAttr" : "displayName", "langTag" : true },
  "preferred_username"     : { "ldapAttr" : "mail" },
  "email"                  : { "ldapAttr" : "mail" },
  "email_verified"         : { "ldapValue" : "true", "jsonType" : "boolean" },
  "phone_number"           : { "ldapAttr" : "telephoneNumber" },
  "phone_number_verified"  : { "ldapValue" : "true", "jsonType" : "boolean" },
  "address.formatted"      : { "ldapAttr" : "postalAddress", "langTag" : true },
  "address.street_address" : { "ldapAttr" : "street", "langTag" : true },
  "address.locality"       : { "ldapAttr" : "l", "langTag" : true },
  "address.region"         : { "ldapAttr" : "st", "langTag" : true },
  "address.postal_code"    : { "ldapAttr" : "postalCode", "langTag" : true },
  "address.country"        : { "ldapAttr" : "co", "langTag" : true }
}
```

Questions or comments? Email support@connect2id.com

