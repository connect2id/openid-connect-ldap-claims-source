package com.nimbusds.openid.connect.provider.spi.claims.ldap;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import javax.servlet.ServletContext;

import org.infinispan.manager.EmbeddedCacheManager;

import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.ServiceContext;


/**
 * Initialisation context implementation for testing purposes.
 */
class MockInitContext implements InitContext {


	@Override
	public InputStream getResourceAsStream(final String path) {

		if (path.equals(LDAPClaimsSource.CONFIG_FILE_PATH)) {

			try {
				return new FileInputStream("test.properties");
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		if (path.equals(LDAPClaimsSource.MAP_FILE_PATH)) {

			try {
				return new FileInputStream("ldapClaimsMap.json");
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		return null;
	}
	
	
	@Override
	public ServletContext getServletContext() {
		return null;
	}
	
	
	@Override
	public EmbeddedCacheManager getInfinispanCacheManager() {
		return null;
	}
	
	
	@Override
	public Issuer getOPIssuer() {
		return null;
	}
	
	
	@Override
	public URI getTokenEndpointURI() {
		return null;
	}
	
	
	@Override
	public ServiceContext getServiceContext() {
		return null;
	}
	
	
	@Override
	public Issuer getIssuer() {
		return null;
	}
}
