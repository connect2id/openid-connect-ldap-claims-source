package com.nimbusds.openid.connect.provider.spi.claims.ldap;


import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;


/**
 * Tests the LDAP claims source.
 */
public class LDAPClaimsSourceTest extends LDAPConnectorTest {


	public void testRun()
		throws Exception {

		LDAPClaimsSource source = new LDAPClaimsSource();

		source.init(new MockInitContext());

		assertTrue(source.isEnabled());

		System.out.println("Supported claims: " + source.supportedClaims());

		Set<String> supportedClaims = source.supportedClaims();

		assertTrue(supportedClaims.contains("sub"));
		assertTrue(supportedClaims.contains("email"));
		assertTrue(supportedClaims.contains("email_verified"));
		assertTrue(supportedClaims.contains("name"));
		assertTrue(supportedClaims.contains("given_name"));
		assertTrue(supportedClaims.contains("family_name"));
		assertTrue(supportedClaims.contains("nickname"));
		assertTrue(supportedClaims.contains("preferred_username"));
		assertTrue(supportedClaims.contains("address"));
		assertTrue(supportedClaims.contains("x_employee_number"));
		assertTrue(supportedClaims.contains("phone_number"));
		assertTrue(supportedClaims.contains("phone_number_verified"));
		assertEquals(12, supportedClaims.size());

		UserInfo userInfo = source.getClaims(new Subject("alice"), new HashSet<>(Arrays.asList("email", "email_verified")), null);

		assertEquals("alice", userInfo.getSubject().getValue());
		assertEquals("alice@wonderland.net", userInfo.getEmailAddress());
		assertTrue(userInfo.getEmailVerified());
	}


	public void testLDAPAttributeMapPassedAsBase64URLEncodedJSONObject()
		throws Exception {

		LDAPClaimsSource source = new LDAPClaimsSource();
		
		String spec =
			"{\n" +
			"  \"sub\"            : { \"ldapAttr\" : \"uid\" },\n" +
			"  \"email\"          : { \"ldapAttr\" : \"mail\" },\n" +
			"  \"email_verified\" : { \"ldapValue\" : \"true\", \"jsonType\" : \"boolean\" }\n" +
			"}";
		
		String b64 = Base64.getEncoder().encodeToString(spec.getBytes(StandardCharsets.UTF_8));
		
		System.out.println(b64);
		
		// override with sys prop
		System.setProperty("op.ldapClaimsSource.attributeMap", b64);

		source.init(new MockInitContext());

		assertTrue(source.isEnabled());

		System.out.println("Supported claims: " + source.supportedClaims());

		Set<String> supportedClaims = source.supportedClaims();

		assertTrue(supportedClaims.contains("sub"));
		assertTrue(supportedClaims.contains("email"));
		assertTrue(supportedClaims.contains("email_verified"));
		assertEquals(3, supportedClaims.size());

		UserInfo userInfo = source.getClaims(new Subject("alice"), new HashSet<>(Arrays.asList("email", "email_verified")), null);

		assertEquals("alice", userInfo.getSubject().getValue());
		assertEquals("alice@wonderland.net", userInfo.getEmailAddress());
		assertTrue(userInfo.getEmailVerified());
		
		System.clearProperty("op.ldapClaimsSource.attributeMap");
	}


	public void testSystemPropertiesOverrideEnable()
		throws Exception {

		LDAPClaimsSource source = new LDAPClaimsSource();

		System.setProperty("op.ldapClaimsSource.enable", "false");

		source.init(new MockInitContext());

		assertFalse(source.isEnabled());

		System.clearProperty("op.ldapClaimsSource.enable");
	}
}
