package com.nimbusds.openid.connect.provider.spi.claims.ldap;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.claims.Address;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.sdk.Entry;
import junit.framework.TestCase;


/**
 * Tests the LDAP connector. Uses an in-memory LDAP directory.
 */
public class LDAPConnectorTest extends TestCase {


	/**
	 * The test in-memory LDAP server.
	 */
	private InMemoryDirectoryServer testLDAPServer;
	
	
	private static Properties getTestProperties() {
		
		Properties props = new Properties();
		
		try {
			props.load(new FileInputStream("test.properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		return props;
	}


	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();

		// Get the configuration
		Configuration config = new Configuration(getTestProperties());

		// Set test LDAP server port
		InMemoryListenerConfig listenerConfig = InMemoryListenerConfig.createLDAPConfig("test-ldap", config.server.url[0].getPort());

		// Set test LDAP server context, user
		InMemoryDirectoryServerConfig dirConfig = new InMemoryDirectoryServerConfig("dc=wonderland,dc=net");
		dirConfig.setListenerConfigs(listenerConfig);
		dirConfig.addAdditionalBindCredentials(config.directory.user.dn.toString(), config.directory.user.password);

		// Start test LDAP server
		testLDAPServer = new InMemoryDirectoryServer(dirConfig);
		testLDAPServer.startListening();

		// Populate initial directory
		testLDAPServer.bind(config.directory.user.dn.toString(), config.directory.user.password);

		Entry entry = new Entry("dc=wonderland,dc=net");
		entry.addAttribute("objectClass", "top", "domain");
		entry.addAttribute("dc", "wonderland");
		testLDAPServer.add(entry);

		entry = new Entry(config.directory.baseDN);
		entry.addAttribute("objectClass", "top", "organizationalUnit");
		testLDAPServer.add(entry);

		entry = new Entry("uid=alice, " + config.directory.baseDN);
		entry.addAttribute("objectClass", "top", "person", "organizationalPerson", "inetOrgPerson");
		entry.addAttribute("cn", "Alice Adams");
		entry.addAttribute("givenName", "Alice");
		entry.addAttribute("sn", "Adams");
		entry.addAttribute("displayName", "Alice");
		entry.addAttribute("mail", "alice@wonderland.net");
		entry.addAttribute("telephoneNumber", "+359 85 100 200 300");
		entry.addAttribute("employeeNumber", "0001");
		entry.addAttribute("postalAddress", "100, Main Street $ Sofia 1000 $ Bulgaria");
		entry.addAttribute("street", "100, Main Street");
		entry.addAttribute("l", "Sofia");
		entry.addAttribute("st", "Sofia");
		entry.addAttribute("postalCode", "1000");
		testLDAPServer.add(entry);
	}


	@Override
	public void tearDown()
		throws Exception {
		
		super.tearDown();

		testLDAPServer.shutDown(true);
	}


	public void testInit()
		throws Exception {

		LDAPClaimsSource ldapSource = new LDAPClaimsSource();
		ldapSource.init(new MockInitContext());
		System.out.println("Completed initialization of the LDAP claims source");
		ldapSource.shutdown();
	}


	public void testSupportedClaims()
		throws Exception {

		LDAPClaimsSource ldapSource = new LDAPClaimsSource();
		ldapSource.init(new MockInitContext());

		Set<String> supportedClaims = ldapSource.supportedClaims();
		assertTrue(supportedClaims.contains("sub"));
		assertTrue(supportedClaims.contains("name"));
		assertTrue(supportedClaims.contains("given_name"));
		assertTrue(supportedClaims.contains("family_name"));
		assertTrue(supportedClaims.contains("nickname"));
		assertTrue(supportedClaims.contains("preferred_username"));
		assertTrue(supportedClaims.contains("email"));
		assertTrue(supportedClaims.contains("email_verified"));
		assertTrue(supportedClaims.contains("phone_number"));
		assertTrue(supportedClaims.contains("phone_number_verified"));
		assertTrue(supportedClaims.contains("x_employee_number"));
		assertTrue(supportedClaims.contains("address"));

		ldapSource.shutdown();
	}


	public void testGetSingleUserInfoClaim()
		throws Exception {

		LDAPClaimsSource ldapSource = new LDAPClaimsSource();
		ldapSource.init(new MockInitContext());

		Set<String> requestedClaims = new HashSet<>();
		requestedClaims.add("email");

		UserInfo userInfo = ldapSource.getClaims(new Subject("alice"), requestedClaims, null);
		System.out.println("UserInfo: " + userInfo.toJSONObject());

		assertEquals(new Subject("alice"), userInfo.getSubject());
		assertEquals("alice@wonderland.net", userInfo.getEmailAddress());
		assertEquals(2, userInfo.toJSONObject().size());

		ldapSource.shutdown();
	}


	public void testGetAllAvailableUserInfo()
		throws Exception {

		LDAPClaimsSource ldapSource = new LDAPClaimsSource();
		ldapSource.init(new MockInitContext());

		Set<String> supportedClaims = ldapSource.supportedClaims();
		System.out.println("UserInfo claims to request: " + supportedClaims);

		UserInfo userInfo = ldapSource.getClaims(new Subject("alice"), supportedClaims, null);
		System.out.println("UserInfo: " + userInfo.toJSONObject());

		assertEquals(new Subject("alice"), userInfo.getSubject());
		assertEquals("Alice Adams", userInfo.getName());
		assertEquals("Alice", userInfo.getGivenName());
		assertEquals("Adams", userInfo.getFamilyName());
		assertEquals("Alice", userInfo.getNickname());
		assertEquals("alice@wonderland.net", userInfo.getPreferredUsername());
		assertEquals("alice@wonderland.net", userInfo.getEmailAddress());
		assertTrue(userInfo.getEmailVerified());
		assertEquals("+359 85 100 200 300", userInfo.getPhoneNumber());
		assertTrue(userInfo.getPhoneNumberVerified());
		assertEquals("0001", userInfo.getStringClaim("x_employee_number"));

		Address address = userInfo.getAddress();
		System.out.println(address.toJSONObject());
		assertEquals("100, Main Street $ Sofia 1000 $ Bulgaria", address.getFormatted());
		assertEquals("100, Main Street", address.getStreetAddress());
		assertEquals("Sofia", address.getLocality());
		assertEquals("Sofia", address.getRegion());
		assertEquals("1000", address.getPostalCode());

		ldapSource.shutdown();
	}


	public void testGetUserInfoInvalidSubject()
		throws Exception {

		LDAPClaimsSource ldapSource = new LDAPClaimsSource();
		ldapSource.init(new MockInitContext());

		Set<String> supportedClaims = ldapSource.supportedClaims();

		assertNull(ldapSource.getClaims(new Subject("bob"), supportedClaims, null));

		ldapSource.shutdown();
	}


	public void testDisabled()
		throws Exception {

		System.setProperty("op.ldapClaimsSource.enable", "false");

		LDAPClaimsSource ldapSource = new LDAPClaimsSource();
		ldapSource.init(new MockInitContext());

		System.clearProperty("op.ldapClaimsSource.enable");

		Set<String> supportedClaims = ldapSource.supportedClaims();

		assertNull(ldapSource.getClaims(new Subject("alice"), supportedClaims, null));

		ldapSource.shutdown();
	}
}
