package com.nimbusds.openid.connect.provider.spi.claims.ldap;


import java.io.FileInputStream;
import java.util.Properties;

import com.nimbusds.common.config.ServerSelectionAlgorithm;
import com.nimbusds.common.ldap.LDAPConnectionSecurity;
import com.unboundid.ldap.sdk.SearchScope;
import junit.framework.TestCase;


/**
 * Tests the configuration class.
 */
public class ConfigurationTest extends TestCase {


	public void testParse() {

		Properties props = new Properties();

		props.setProperty("op.ldapClaimsSource.enable", "true");
		
		props.setProperty("op.ldapClaimsSource.server.url", "ldap://localhost:1389 ldap://remotehost:1389");
		props.setProperty("op.ldapClaimsSource.server.selectionAlgorithm", "FAILOVER");
		props.setProperty("op.ldapClaimsSource.server.connectTimeout", "1000");
		props.setProperty("op.ldapClaimsSource.server.security", "StartTLS");
		props.setProperty("op.ldapClaimsSource.server.trustSelfSignedCerts", "true");
		props.setProperty("op.ldapClaimsSource.server.connectionPoolSize", "10");
		props.setProperty("op.ldapClaimsSource.server.connectionPoolMaxWaitTime", "100");
		props.setProperty("op.ldapClaimsSource.server.connectionMaxAge", "3600000");

		props.setProperty("op.ldapClaimsSource.directory.user.dn", "cn=Directory Manager");
		props.setProperty("op.ldapClaimsSource.directory.user.password", "secret");

		props.setProperty("op.ldapClaimsSource.directory.baseDN", "ou=people,dc=wonderland,dc=net");
		props.setProperty("op.ldapClaimsSource.directory.scope", "ONE");
		props.setProperty("op.ldapClaimsSource.directory.filter", "(uid=%u)");

		props.setProperty("op.ldapClaimsSource.customTrustStore.enable", "false");
		props.setProperty("op.ldapClaimsSource.customKeyStore.enable", "false");

		Configuration config = new Configuration(props);

		assertTrue(config.enable);
		
		assertEquals(LDAPClaimsSource.MAP_FILE_PATH, config.attributeMap);

		assertEquals("ldap://localhost:1389", config.server.url[0].toString());
		assertEquals("ldap://remotehost:1389", config.server.url[1].toString());
		assertEquals(1000, config.server.connectTimeout);
		assertEquals(ServerSelectionAlgorithm.FAILOVER, config.server.selectionAlgorithm);
		assertEquals(LDAPConnectionSecurity.STARTTLS, config.server.security);
		assertTrue(config.server.trustSelfSignedCerts);
		assertEquals(10, config.server.connectionPoolSize);
		assertEquals(100, config.server.connectionPoolMaxWaitTime);
		assertEquals(3600000L, config.server.connectionMaxAge);

		assertEquals("cn=Directory Manager", config.directory.user.dn.toString());
		assertEquals("secret", config.directory.user.password);

		assertEquals("ou=people,dc=wonderland,dc=net", config.directory.baseDN.toString());
		assertEquals(SearchScope.ONE, config.directory.scope);
		assertEquals("(uid=%u)", config.directory.filter.getTemplate());

		assertFalse(config.customTrustStore.enable);

		assertFalse(config.customKeyStore.enable);
	}


	public void testParseTestProperties()
		throws Exception {

		Properties props = new Properties();
		props.load(new FileInputStream("test.properties"));

		Configuration config = new Configuration(props);

		assertTrue(config.enable);
		
		assertEquals(LDAPClaimsSource.MAP_FILE_PATH, config.attributeMap);

		assertEquals("ldap://localhost:40389", config.server.url[0].toString());
		assertEquals(1000, config.server.connectTimeout);
		assertNull(config.server.selectionAlgorithm);
		assertEquals(LDAPConnectionSecurity.NONE, config.server.security);
		assertTrue(config.server.trustSelfSignedCerts);
		assertEquals(10, config.server.connectionPoolSize);
		assertEquals(250, config.server.connectionPoolMaxWaitTime);
		assertEquals(0L, config.server.connectionMaxAge);

		assertEquals("cn=Directory Manager", config.directory.user.dn.toString());
		assertEquals("secret", config.directory.user.password);

		assertEquals("ou=people,dc=wonderland,dc=net", config.directory.baseDN.toString());
		assertEquals(SearchScope.ONE, config.directory.scope);
		assertEquals("(uid=%u)", config.directory.filter.getTemplate());

		assertFalse(config.customTrustStore.enable);

		assertFalse(config.customKeyStore.enable);
	}
	
	
	public void testDisabledByDefault() {
		
		Configuration config = new Configuration(new Properties());
		
		assertFalse(config.enable);
		assertNull(config.attributeMap);
		assertNull(config.server);
		assertNull(config.directory);
		assertNull(config.customTrustStore);
		assertNull(config.customKeyStore);
		
		config.log();
	}
	
	
	public void testExplicitAttributeMap()
		throws Exception {
		
		Properties props = new Properties();
		props.load(new FileInputStream("test.properties"));
		// json object
		props.setProperty("op.ldapClaimsSource.attributeMap", "{}");
		
		Configuration config = new Configuration(props);
		assertTrue(config.enable);
		assertEquals("{}", config.attributeMap);
		
		config.log();
	}
}
