package com.nimbusds.openid.connect.provider.spi.claims.ldap;


import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Base64;

import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import junit.framework.TestCase;
import net.minidev.json.JSONObject;


public class LDAPAttributeMapLoaderTest extends TestCase {
	
	
	public void testDetectFilePath() {
		
		assertTrue(LDAPAttributeMapLoader.isFilePath("/WEB-INF/attributeMap.json"));
		assertTrue(LDAPAttributeMapLoader.isFilePath("attributeMap.json"));
		
		assertFalse(LDAPAttributeMapLoader.isFilePath("{}"));
	}
	
	
	public void testDetectJSONObject() {
		
		assertTrue(LDAPAttributeMapLoader.isJSONObject("{}"));
		assertTrue(LDAPAttributeMapLoader.isJSONObject(" {} "));
		assertTrue(LDAPAttributeMapLoader.isJSONObject("{"));
		
		assertFalse(LDAPAttributeMapLoader.isJSONObject("[]"));
		assertFalse(LDAPAttributeMapLoader.isJSONObject(" [] "));
		assertFalse(LDAPAttributeMapLoader.isJSONObject("["));
	}
	
	
	public void testLoadResource()
		throws Exception {
		
		JSONObject jsonObject = LDAPAttributeMapLoader.load(LDAPClaimsSource.MAP_FILE_PATH, new MockInitContext());
		
		// Compare with expected test JSON object
		String json = new String(Files.readAllBytes(new File("ldapClaimsMap.json").toPath()), StandardCharsets.UTF_8);
		assertEquals(JSONObjectUtils.parse(json), jsonObject);
	}
	
	
	
	public void testLoadJSONObject()
		throws Exception {
	
		JSONObject jsonObject = LDAPAttributeMapLoader.load("{}", new MockInitContext());
		
		assertTrue(jsonObject.isEmpty());
	}
	
	
	
	public void testLoadBase64EncodedJSONObject()
		throws Exception {
		
		String b64 = Base64.getEncoder().encodeToString("{}".getBytes(StandardCharsets.UTF_8));
		
		JSONObject jsonObject = LDAPAttributeMapLoader.load(b64, new MockInitContext());
	
		assertTrue(jsonObject.isEmpty());
	}
}
