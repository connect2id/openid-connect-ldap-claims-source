package com.nimbusds.openid.connect.provider.spi.claims.ldap;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.stream.Collectors;

import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * LDAP attribute map loader.
 */
class LDAPAttributeMapLoader {
	
	
	/**
	 * Returns {@code true} if the specifies string is likely a file path.
	 *
	 * @param s The string.
	 *
	 * @return {@code true} for a file path, else {@code false}.
	 */
	static boolean isFilePath(final String s) {
		
		return s != null && (s.contains("/") || s.contains("."));
	}
	
	
	/**
	 * Returns {@code true} if the specified string is likely a JSON object
	 * string.
	 *
	 * @param s The string.
	 *
	 * @return {@code true} for a JSON object string, else {@code false}.
	 */
	static boolean isJSONObject(final String s) {
		
		return s != null && s.trim().startsWith("{");
	}
	
	
	
	/**
	 * Loads the specified resource as a UTF-encoded string.
	 *
	 * @param resourcePath The resource path. Must not be {@code null}.
	 * @param initContext  The initialisation context. Must not be
	 *                     {@code null}.
	 *
	 * @return The resource as a UTF-8 encoded string.
	 *
	 * @throws Exception If loading failed.
	 */
	static String loadResource(final String resourcePath,
				   final InitContext initContext)
		throws Exception {
		
		InputStream is = initContext.getResourceAsStream(resourcePath);
		
		if (is == null) {
			throw new Exception("Couldn't find LDAP attribute map resource: " + resourcePath);
		}
		
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
			return buffer.lines().collect(Collectors.joining("\n"));
		}
	}
	
	
	/**
	 * Loads an LDAP attribute map.
	 *
	 * @param spec        The spec.
	 * @param initContext The initialisation context. Must not be
	 *                    {@code null}.
	 *
	 * @return The LDAP attribute map as a JSON object.
	 *
	 * @throws Exception If loading failed.
	 */
	
	static JSONObject load(final String spec, final InitContext initContext)
		throws Exception {
		
		Logger log = LogManager.getLogger("MAIN");
		
		String jsonText;
		
		if (isJSONObject(spec)) {
			log.info("[CSLDAP0100] Loading LDAP attribute map from inline JSON object");
			jsonText = spec;
		} else if (isFilePath(spec)) {
			log.info("[CSLDAP0100] Loading LDAP attribute map from file resource");
			jsonText = loadResource(spec, initContext);
		} else {
			log.info("[CSLDAP0100] Loading LDAP attribute map from inline BASE-64 encoded JSON object");
			byte[] bytes = Base64.getDecoder().decode(spec);
			jsonText = new String(bytes, StandardCharsets.UTF_8);
		}
		
		try {
			return JSONObjectUtils.parse(jsonText);
		} catch (Exception e) {
			throw new Exception("Couldn't load LDAP attribute map resource: " + e.getMessage(), e);
		}
	}
}
