/**
 * LDAP connector for retrieving OpenID Connect UserInfo claims.
 */
package com.nimbusds.openid.connect.provider.spi.claims.ldap;